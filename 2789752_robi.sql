-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: fdb20.awardspace.net
-- Generation Time: Jul 29, 2018 at 12:50 PM
-- Server version: 5.7.20-log
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `2789752_robi`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `car_id` int(11) NOT NULL,
  `car_brand` varchar(10) NOT NULL,
  `car_model` varchar(50) NOT NULL,
  `plate_number` varchar(10) NOT NULL,
  `production_year` int(4) NOT NULL,
  `displacement` varchar(10) NOT NULL,
  `horse_power` int(5) NOT NULL,
  `user_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`car_id`, `car_brand`, `car_model`, `plate_number`, `production_year`, `displacement`, `horse_power`, `user_id`) VALUES
(4, 'WV', 'golf 2', 'ns123ss', 1980, '1300', 75, 42),
(5, 'Alfa Romeo', '147', 'NS273YS', 2001, '1600', 105, 43),
(6, 'Suzuki', 'Swift', 'NS256XT', 1997, '1000', 50, 46);

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

CREATE TABLE `problems` (
  `problem_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `plate_number` varchar(10) CHARACTER SET utf8 NOT NULL,
  `status` varchar(10) CHARACTER SET utf8 NOT NULL,
  `note` varchar(250) CHARACTER SET utf8 NOT NULL,
  `total_price` int(10) NOT NULL,
  `date_start` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `reservation_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `problems`
--

INSERT INTO `problems` (`problem_id`, `user_id`, `plate_number`, `status`, `note`, `total_price`, `date_start`, `date_end`, `reservation_id`) VALUES
(1, 42, 'ns123ss', 'pending', 'asd', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(2, 43, 'NS273YS', 'pending', 'Dasdsadas sadfsafsd fsdfsd fsd', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(3, 43, 'NS273YS', 'pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(4, 43, 'NS273YS', 'pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(5, 43, 'NS273YS', 'pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(6, 43, 'NS273YS', 'pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(7, 43, 'NS273YS', 'pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(8, 43, 'NS273YS', 'pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(9, 43, 'NS273YS', 'pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(10, 43, 'NS273YS', 'pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(11, 43, 'NS273YS', 'pending', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(12, 43, 'NS273YS', 'pending', '', 70, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(13, 43, 'NS273YS', 'pending', '', 70, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0),
(20, 46, 'NS256XT', 'pending', '', 310, '2018-07-31 12:00:00', '2018-07-31 16:30:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `problems_service`
--

CREATE TABLE `problems_service` (
  `problem_id` int(11) NOT NULL,
  `repair_id` int(11) NOT NULL,
  `client_comment` varchar(250) CHARACTER SET utf8 NOT NULL,
  `worker_comment` varchar(250) CHARACTER SET utf8 NOT NULL,
  `price` int(10) NOT NULL,
  `duration` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `problems_service`
--

INSERT INTO `problems_service` (`problem_id`, `repair_id`, `client_comment`, `worker_comment`, `price`, `duration`) VALUES
(1, 1, '', '', 0, '00:00:00'),
(1, 2, '', '', 0, '00:00:00'),
(1, 3, '', '', 0, '00:00:00'),
(2, 1, '', '', 0, '00:00:00'),
(2, 3, '', '', 0, '00:00:00'),
(3, 3, '', '', 0, '00:00:00'),
(4, 3, '', '', 0, '00:00:00'),
(5, 3, '', '', 0, '00:00:00'),
(5, 4, '', '', 0, '00:00:00'),
(6, 3, '', '', 0, '00:00:00'),
(6, 4, '', '', 0, '00:00:00'),
(7, 3, '', '', 0, '00:00:00'),
(7, 4, '', '', 0, '00:00:00'),
(8, 3, '', '', 0, '00:00:00'),
(8, 4, '', '', 0, '00:00:00'),
(9, 3, '', '', 0, '00:00:00'),
(9, 4, '', '', 0, '00:00:00'),
(10, 3, '', '', 0, '00:00:00'),
(10, 4, '', '', 0, '00:00:00'),
(11, 3, '', '', 0, '00:00:00'),
(11, 4, '', '', 0, '00:00:00'),
(12, 3, '', '', 0, '00:00:00'),
(12, 4, '', '', 0, '00:00:00'),
(13, 3, '', '', 0, '00:00:00'),
(13, 4, '', '', 0, '00:00:00'),
(14, 1, '', '', 0, '00:00:00'),
(15, 1, '', '', 0, '00:00:00'),
(15, 6, '', '', 0, '00:00:00'),
(16, 1, '', '', 0, '00:00:00'),
(16, 6, '', '', 0, '00:00:00'),
(17, 1, '', '', 0, '00:00:00'),
(17, 6, '', '', 0, '00:00:00'),
(18, 1, '', '', 0, '00:00:00'),
(18, 6, '', '', 0, '00:00:00'),
(19, 1, '', '', 0, '00:00:00'),
(19, 6, '', '', 0, '00:00:00'),
(20, 1, '', '', 0, '00:00:00'),
(20, 6, '', '', 0, '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `repairs`
--

CREATE TABLE `repairs` (
  `repair_id` int(11) NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `price` int(10) NOT NULL,
  `duration` time NOT NULL,
  `description` varchar(250) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `repairs`
--

INSERT INTO `repairs` (`repair_id`, `name`, `price`, `duration`, `description`) VALUES
(1, 'small service', 10, '00:30:00', 'change of motor oil and all filters'),
(2, 'big service', 30, '01:00:00', 'change of motor oil, all filters, belts and spark plugs/warmer'),
(3, 'brakes', 20, '00:30:00', 'fixing the brakes'),
(4, 'clutch', 50, '01:00:00', 'change of clutch parts'),
(5, 'gearbox', 100, '02:00:00', 'repair of gearbox'),
(6, 'engine', 300, '04:00:00', 'engine reaplacement');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_first` varchar(255) NOT NULL,
  `user_last` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_uid` varchar(255) NOT NULL,
  `user_pwd` varchar(32) NOT NULL,
  `user_salt` varchar(12) NOT NULL,
  `reg_date` datetime NOT NULL,
  `role_id` int(1) NOT NULL,
  `phoneNum` int(15) NOT NULL,
  `zipCode` int(10) NOT NULL,
  `city` varchar(50) NOT NULL,
  `adress` varchar(100) NOT NULL,
  `active` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_first`, `user_last`, `user_email`, `user_uid`, `user_pwd`, `user_salt`, `reg_date`, `role_id`, `phoneNum`, `zipCode`, `city`, `adress`, `active`) VALUES
(1, 'admin', 'admin', 'admin@asd.asd', 'admin', '8a560898969f9d8d26b004c230e1598e', 'ljtcwx6hzb7m', '2018-07-15 12:23:02', 1, 15466556, 0, '', '', '1'),
(42, 'korisnik', 'korisnik', 'sitescadu@gmail.com', 'korisnik1', 'd21e01bf35abfb1064652a8acaff5fea', 'q5uh1m0oxuyd', '2018-07-25 09:57:41', 2, 2147483647, 0, '', '', '1'),
(43, 'Jovan', 'Nanic', 'cheevt@gmail.com', 'CheeVT', '7610226a723d8c06c6624b62614d8516', '_kzxf-4!z34m', '2018-07-25 17:44:32', 2, 65564584, 0, '', '', '1'),
(44, 'marko', 'marko', 'marko@gmail.com', 'marko', 'd14e65cff95144b9fa53ca323f5591fa', 'a_bfzfplh4ii', '2018-07-25 17:54:29', 2, 6584855, 0, '', '', ''),
(46, 'Jovan', 'Nanic', 'vtchee@gmail.com', 'VTChee', '9c7430fcf27b82999b9f2c360f5619ab', '6p5r_sjj8ce8', '2018-07-29 10:07:45', 2, 66656565, 0, '', '', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`car_id`);

--
-- Indexes for table `problems`
--
ALTER TABLE `problems`
  ADD PRIMARY KEY (`problem_id`);

--
-- Indexes for table `repairs`
--
ALTER TABLE `repairs`
  ADD PRIMARY KEY (`repair_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `car_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `problems`
--
ALTER TABLE `problems`
  MODIFY `problem_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `repairs`
--
ALTER TABLE `repairs`
  MODIFY `repair_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
