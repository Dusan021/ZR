

	<!-- OUR TEAM -->
	<section id="ourTeam">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="section-title">
						<h1>our team</h1>
						<span class="st-border"></span>
					</div>
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="team-member">
						<div class="member-image">
							<img class="img-responsive" src="images/members/robi.jpg" alt="">
							<div class="member-social">
								<a href="https://www.facebook.com/robert.dobosi.7" target="_blank"><i class="fa fa-facebook"></i></a>
							</div>
						</div>
						<div class="member-info">
							<h4>robert doboši</h4>
							<span>the boss</span>
						</div>
					</div>
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="team-member">
						<div class="member-image">
							<img class="img-responsive" src="images/members/maja.jpg" alt="">
							<div class="member-social">
								<a href="https://www.facebook.com/marjamaja.dobosi" target="_blank"><i class="fa fa-facebook"></i></a>
							</div>
						</div>
						<div class="member-info">
							<h4>marija maja dobosi</h4>
							<span>sale manager</span>
						</div>
					</div>
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="team-member">
						<div class="member-image">
							<img class="img-responsive" src="images/members/majstor1.jpg" alt="">
							<div class="member-social">
								<a href="" target="_blank"><i class="fa fa-facebook"></i></a>
							</div>
						</div>
						<div class="member-info">
							<h4>kristijan kocić</h4>
							<span>main mechanic</span>
						</div>
					</div>
				</div>

				<div class="col-md-3 col-sm-6">
					<div class="team-member">
						<div class="member-image">
							<img class="img-responsive" src="images/members/majstor2.jpg" alt="">
							<div class="member-social">
								<a href="" target="_blank"><i class="fa fa-facebook"></i></a>
							</div>
						</div>
						<div class="member-info">
							<h4>nikola zarić</h4>
							<span>mechanic</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /OUR TEAM -->
