<?php include('../login/includes/session.inc.php');
include('../login/includes/dbh.inc.php');
//count users
$sql = "SELECT count(user_id) AS total FROM users WHERE role_id = '2';";
$result = mysqli_query($conn, $sql);
$values = mysqli_fetch_assoc($result);
$users = $values['total'];
//count repairs
$sql = "SELECT count(repair_id) AS total FROM repairs ";
$result = mysqli_query($conn, $sql);
$values = mysqli_fetch_assoc($result);
$repairs = $values['total'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <meta name="author" content="">

	<title>Auto Servis Robi</title>

  <!-- Main CSS file -->
	<link rel="stylesheet" href="../css/bootstrap.min.css" />
	<link rel="stylesheet" href="../css/font-awesome.css" />
	<link rel="stylesheet" href="../css/style.css" />
	<link rel="stylesheet" href="../css/responsive.css" />


	<!-- Favicon -->
	<link rel="shortcut icon" href="images/icon/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/icon/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/icon/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/icon/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="../images/icon/apple-touch-icon-57-precomposed.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

  <body>
    <header>
      <nav  class="navbar st-navbar navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#st-navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
          <a class="logo" href="index.php"><img src="../images/robi_svg_dynamic.png" alt=""></a>
          </div>
          <div class="collapse navbar-collapse" id="st-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../index.php">home</a></li>
                <li><a href="../index.php">about us</a></li>
                <li><a href="../index.php">prices</a></li>
                <li><a href="../index.php">contact</a></li>
                <li><a href="../index.php">our team</a></li>


                <div class="nav-login" style=" margin-left: 110px;">
                    <?php
                    if (isset($_SESSION['u_id']) || isset($_SESSION['role_id'])) {
                        $name = $_SESSION['u_uid'];

                        echo '<form style="float: right; margin-right: 15px;" class="logoutform" action="../login/includes/logout.inc.php" method="post">
                <span>you are logged in as <b>' . $name . '</b></span>
                <button type="submit" name="submit">logout</button>
                </form>';
                    } else if (isset($_COOKIE['u_id']) && $_COOKIE['role_id']) {
                        $name = $_COOKIE['u_uid'];

                        echo '<form class="logoutform" action="login/includes/logout.inc.php" method="post">
                <span>you are logged in as <b>' . $name . '</b></span>
                <button type="submit" name="submit">logout</button>';
                    }
                    ?>

                </div> <!--end of nav-login  -->
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
      </nav>
    </header>
    <h1 style="margin-top: 100px;"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>dashboard  <small>manage your site</small> </h1>
    <section id="main">
      <a href="index.html" ><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> dashboard  </a>
      <a href="pages.html" ><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> pages <span class="badge">12</span></a>
      <a href="#" id="repairs" ><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> repairs list/add <span class="badge"><?php echo $repairs; ?></span></a>
      <a href="#" id="users" ><span class="glyphicon glyphicon-user" aria-hidden="true"></span> users <span class="badge"><?php echo $users; ?></span></a><br /><br />
<!-- WEBSITE OVERVIEW -->
      <h3 class="panel-title">website overview</h3>
      <h2><span class="glyphicon glyphicon-user"  aria-hidden="true"><?php echo $users; ?></span></h2>
       <h4>Users</h4>
      <h2><span  class="glyphicon glyphicon-list-alt" aria-hidden="true">12</span></h2>
      <h4>Pages</h4>
      <h2><span class="glyphicon glyphicon-pencil" aria-hidden="true"><?php echo $repairs ?></span></h2>
      <h4>repairs</h4>
      <h2><span class="glyphicon glyphicon-stats" aria-hidden="true">12,334</span></h2>
      <h4>Visitors</h4>

<!--USERS LIST  -->
      <div class="users">
       <p>ajax</p>
      </div><!-- end of users -->
<!-- SERVICE ADD -->
    <div class="" style="clear: both;">
      <h3>add repair</h3>
      <form class="" action="includes/addRepair.inc.php" method="post">
        <input type="text" name="name" placeholder="repair-name"> <br />
        <input type="text" name="price" placeholder="price"><br />
        <input type="text" name="duration" placeholder="duration 00:00"><br />
        <textarea name="description" rows="11" cols="40"  placeholder="description"></textarea>
        <button type="submit" name="submit">add</button>
      </form>
    </div>
  </section><!-- end of main -->
  <footer id="footer">
    <p>coyright adminStrap &copy; 2018</p>
  </footer>

  <!-- JS -->
	<script type="text/javascript" src="../js/jquery.min.js"></script><!-- jQuery -->
	<script type="text/javascript" src="../js/bootstrap.min.js"></script><!-- Bootstrap -->
	<script type="text/javascript" src="../js/jquery.counterup.min.js"></script><!-- CounterUp -->
	<script type="text/javascript" src="../js/scripts.js"></script><!-- Scripts -->
  </body>
</html>
