<?php
if (isset($_POST['submit'])) {
  include_once "../../login/includes/dbh.inc.php";
  session_start();
  $repair = mysqli_real_escape_string($conn, $_POST['name']);
  $price = mysqli_real_escape_string($conn, $_POST['price']);
  $duration = mysqli_real_escape_string($conn, $_POST['duration']);
  $description = mysqli_real_escape_string($conn, $_POST['description']);
  $user_id = $_SESSION['u_id'];
  //cheking if empty
  if (empty($repair) || empty($price) || empty($duration) || empty($description)) {
    header("Location: ../index.php?registerYourCar=empty"); //err msg says empty
    exit();
  }else{
    //cheking chars
    if (!preg_match("/^[a-zA-Z\s]*$/", $repair) || !preg_match("/^[0-9]*$/", $price) || !preg_match("/^[a-zA-Z0-9:-]*$/", $duration) || !preg_match("/^[a-zA-Z0-9\s,\/]*$/", $description) ) {
      header("Location: ../index.php?registerYourCar=chars"); //err msg says invalid
      exit();
    }else {
      $sql = "INSERT INTO repairs (name, price, duration, description)
              VALUES ('$repair', '$price', '$duration', '$description');";
      mysqli_query($conn, $sql);
      header("Location: ../index.php?registerYourCar=u_sucsess"); //err msg says sucsess
      exit();
      }
    }
  }
  else {
    header("Location: Location: ../index.php");
    exit();
  }
?>
