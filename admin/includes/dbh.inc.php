<?php
  $dbServername = "localhost";
  $dbUsername = "root";
  $dbPassword = "";
  $dbName = "projekatbaza";

  $conn = mysqli_connect($dbServername, $dbUsername, $dbPassword, $dbName);
  if(mysqli_connect_errno()){
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

  mysqli_query($conn,"SET NAMES utf8") or die(mysqli_error($conn));
  mysqli_query($conn,"SET CHARACTER SET utf8") or die(mysqli_error($conn));
  mysqli_query($conn,"SET COLLATION_CONNECTION= 'utf8_general_ci'") or die(mysqli_error($conn));
 ?>
