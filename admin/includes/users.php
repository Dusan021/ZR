<?php include('../login/includes/session.inc.php');
include('../login/includes/dbh.inc.php'); ?>
<!-- LATEST USERS -->
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Latest Users</h3>
  </div>
  <div class="panel-body">
    <?php
      $sql = "SELECT * FROM users LEFT JOIN cars ON users.user_id = cars.user_id WHERE role_id = '2' ORDER BY users.user_id, cars.user_id  ASC;";
      $result = mysqli_query($conn, $sql);
      $resultCheck = mysqli_num_rows($result);
      if ($resultCheck > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
          echo "<div class = 'table_u'>
                  <div class = 'all'>
                    <div class = 'person'>
                      <div class ='left'>
                        <div class = 'info'>user id</div>
                        <div class = 'info'>user name</div>
                        <div class = 'info'>first name</div>
                        <div class = 'info'>last name</div>
                        <div class = 'info'>email</div>
                        <div class = 'info'>reg date</div>
                        <div class = 'info'>phone number</div>
                        <div class = 'info'>zip code</div>
                        <div class = 'info'>city</div>
                        <div class = 'info'>adress</div>
                      </div>
                      <div class = 'right'>
                        <div class =info>". $row['user_id'] ."</div>
                        <div class =info>". $row['user_uid'] ."</div>
                        <div class =info>". $row['user_first'] ."</div>
                        <div class =info>". $row['user_last'] ."</div>
                        <div class =info>". $row['user_email'] ."</div>
                        <div class =info>". $row['reg_date'] ."</div>
                        <div class =info>". $row['phoneNum'] ."</div>
                        <div class =info>". $row['zipCode'] ."</div>
                        <div class =info>". $row['city'] ."</div>
                        <div class =info>". $row['adress'] ."</div>
                      </div>
                    </div>
                    <div class = 'car'>
                      <div class ='leftCar'>
                        <div class = 'info'>car brand</div>
                        <div class = 'info'>car model</div>
                        <div class = 'info'>plate number</div>
                        <div class = 'info'>prod year</div>
                        <div class = 'info'>displacement</div>
                        <div class = 'info'>horse power</div>
                      </div>
                      <div class = 'rightCar'>
                        <div class =info>". $row['car_brand'] ."</div>
                        <div class =info>". $row['car_model'] ."</div>
                        <div class =info>". $row['plate_number'] ."</div>
                        <div class =info>". $row['production_year'] ."</div>
                        <div class =info>". $row['displacement'] ."</div>
                        <div class =info>". $row['horse_power'] ."</div>
                      </div>
                    </div>
                  </div>
                </div>";
        }
      }
?>
