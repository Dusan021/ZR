<?php include('../login/includes/session.inc.php');
include('../../includes/dbh.inc.php'); ?>
<div class="listRepairs">
  <table>
    <tr>
      <td>repair id</td>
      <td>repair name</td>
      <td>price</td>
      <td>duration</td>
      <td>description</td>
    </tr>
  <?php
      $sql = "SELECT * FROM repairs ;";
      $result = mysqli_query($conn, $sql);
      $resultCheck = mysqli_num_rows($result);
      if ($resultCheck > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
          echo "<tr>
                  <td>" . $row['repair_id'] . "</td>
                  <td>" . $row['name'] . "</td>
                  <td>" . $row['price'] . " eur </td>
                  <td>" . $row['duration'] . "</td>
                  <td>" . $row['description'] . "</td>
                </tr>";
        }
      }
    ?>
  </table>
</div>
