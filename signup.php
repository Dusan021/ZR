<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <html lang="en" dir="ltr">
 <head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
   <meta name="description" content="">
   <meta name="author" content="">

   <title>Auto Servis Robi</title>

   <!-- Main CSS file -->
   <link rel="stylesheet" href="css/bootstrap.min.css" />
   <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
   <script type="text/javascript" src="node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
   <link rel="stylesheet" href="css/font-awesome.css" />
   <link rel="stylesheet" href="css/style.css" />
   <link rel="stylesheet" href="css/responsive.css" />



   <!-- Favicon -->
   <link rel="shortcut icon" href="images/icon/favicon.png">
   <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/icon/apple-touch-icon-144-precomposed.png">
   <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/icon/apple-touch-icon-114-precomposed.png">
   <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/icon/apple-touch-icon-72-precomposed.png">
   <link rel="apple-touch-icon-precomposed" href="images/icon/apple-touch-icon-57-precomposed.png">

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
   <![endif]-->

 </head>

   <body>
     <header  id="header">
       <nav class="navbar st-navbar navbar-fixed-top">
         <div class="container">
           <div class="navbar-header">
             <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#st-navbar-collapse">
               <span class="sr-only">Toggle navigation</span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
             </button>
             <a class="logo" href="index.php"><img src="images/logo.png" alt=""></a>
           </div>

           <div class="collapse navbar-collapse" id="st-navbar-collapse">
               <ul class="nav navbar-nav navbar-right">
           <li><a href="index.php">home</a></li>
                 <div class="nav-login">
                     <?php
                     if (isset($_SESSION['u_id']) || isset($_SESSION['role_id'])) {
                         $name = $_SESSION['u_uid'];

                         echo '<form style="float: right; margin-right: 15px;" class="logoutform" action="login/includes/logout.inc.php" method="post">
                 <span>you are logged in as <b>' . $name . '</b></span>
                 <button type="submit" name="submit">logout</button>
                 </form>';
                     } else if (isset($_COOKIE['u_id']) && $_COOKIE['role_id']) {
                         $name = $_COOKIE['u_uid'];

                         echo '<form class="logoutform" action="login/includes/logout.inc.php" method="post">
                 <span>you are logged in as <b>' . $name . '</b></span>
                 <button type="submit" name="submit">logout</button>';
                     }
                     ?>

                 </div> <!--end of nav-login  -->
             </ul>
</div><!-- /.navbar-collapse -->
         </div><!-- /.container -->
       </nav>
     </header>
    <section class="main-container">
      <div  style=" margin-top: 200px;" class="main-wrapper">
        <h2>signup</h2>
        <form id="userForm" class="signup-form" action="login/includes/signup.inc.php" method="post">

          <input type="text" name="uid" placeholder="username">
          <input type="password" name="pwd" placeholder="password">
          <input type="text" name="first" placeholder="fistname">
          <input type="text" name="last" placeholder="lastname">
          <input type="text" name="email" placeholder="e-mail">

          <input type="text" name="phone" placeholder="phone number">
          <input type="text" name="zipCode" placeholder="zip code">
          <input type="text" name="city" placeholder="city">
          <input type="text" name="adress" placeholder="adress">

          <button type="button" name="button"><a href="index.php">home</a> </button>

          <button type="submit" name="submit">sign up</button>
          <script type="text/javascript">
            $("#userForm").validate({
              rules: {
                uid: {
                  required: true,
                  minlength: 5,
                  maxlength: 20
                },
                pwd: {
                  required: true,
                  minlength: 5,
                  maxlength: 20

                },
                first: {
                  required: true,
                  minlength: 5,
                  maxlength: 20
                },
                last: {
                  required: true,
                  minlength: 5,
                  maxlength: 20
                },
                email: {
                  minlength: 3,
                  maxlength: 50,
                  required: true,
                  email: true


                },
                phone: {
                  required: true,
                  minlength: 7,
                  maxlength: 15,
                  number: true
                }
              },
              messages: {
                uid: {
                  required: "This is  required field",
                  minlength:"Minimum length is 5 letters",
                  maxlength: "Maximim length is 10 letters"
                },
                pwd: {
                  required: "This is required field",
                  minlength:"Minimum length is 5 letters",
                  maxlength: "Maximim length is 10 letters"

                },
                first: {
                  required: "This is required field",
                  minlength:"Minimum length is 5 letters",
                  maxlength: "Maximim length is 10 letters"
                },
                last: {
                  required: "This is required field",
                  minlength:"Minimum length is 5 letters",
                  maxlength: "Maximim length is 10 letters"
                },
                email: {
                    minlength:"Minimum length is 3 letters",
                    maxlength: "Maximim length is 50 letters",
                    required: "This is required field",
                    email:"Invalid email"
                },
                phone: {
                  required: "This is required field",
                  minlength:"Minimum length is 7 letters",
                  maxlength: "Maximim length is 15 letters",
                  number: "only numbers allowed"
                }
              }
            });
          </script>

          <?php

            if (!isset($_GET['signup'])) {
              exit();
            }else {
              $signupCheck = $_GET['signup'];
              if ($signupCheck == "empty") {
                echo "<p class='errorr'>you didn't fill out all fields!</p>";
                exit();
              }elseif ($signupCheck == "chars") {
                echo "<p class='errorr'>you  used invalid characters!</p>";
                exit();
              }elseif ($signupCheck == "email") {
                echo "<p class='errorr'>you  used an invalid e-mail!</p>";
                exit();
              }elseif ($signupCheck == "emailtaken") {
                echo "<p class='errorr'>email taken</p>";
                exit();
              }elseif ($signupCheck == "user") {
                echo "<p class='errorr'>you  used an invalid user name!</p>";
                exit();
              }elseif ($signupCheck == "usertaken") {
                echo "<p class='errorr'>user taken!</p>";
                exit();
              }elseif ($signupCheck == "phone_taken") {
                echo "<p class='errorr'>phone taken!</p>";
                exit();
              }elseif ($signupCheck == "u_sucsess") {
                echo "<p class='sucsess'>you have been signed up!</p>";
                exit();
              }


          }
           ?>



        </form>


      </div>
    </section>

  </body>
</html>
