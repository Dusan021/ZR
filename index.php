<?php session_start();
require("login/includes/dbh.inc.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

	<title>Auto Servis Robi</title>

	<!-- Main CSS file -->
	<link rel="stylesheet" href="css/bootstrap.min.css" />
	<link rel="stylesheet" href="css/font-awesome.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/responsive.css" />
<!-- FAVICON -->
	<link rel="shortcut icon" href="images/icon/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/icon/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/icon/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/icon/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="images/icon/apple-touch-icon-57-precomposed.png">
	<!-- FAVICON -->

</head>
<body>

	<!-- PRELOADER -->
	<div id="st-preloader">
		<div id="pre-status">
			<div class="preload-placeholder"></div>
		</div>
	</div>
	<!-- /PRELOADER -->


	<!-- HEADER -->
	<?php include_once("header.php"); ?>
	<!-- /HEADER -->


	<!-- SLIDER -->
	<section id="slider">
		<div id="home-carousel" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner">
				<div class="item active" style="background-image: url(images/slider/01.jpg)">
					<div class="carousel-caption container">
						<div class="row">
							<div class="col-sm-7">
								<h1>Feel free to visit us</h1>
								<h2>Auto Service Robi</h2>
								<p>We take care of your vehicle as if it’s our own</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item" style="background-image: url(images/slider/02.jpg)">
					<div class="carousel-caption container">
						<div class="row">
							<div class="col-sm-7">
								<h1>Best solutions for your vehicle</h1>
								<h2>Expert advices</h2>
								<p>Advices and services about any technical issues</p>
							</div>
						</div>
					</div>
				</div>
				<div class="item" style="background-image: url(images/slider/03.jpg)">
					<div class="carousel-caption container">
						<div class="row">
							<div class="col-sm-7">
								<h1>Change color of your beauty on wheels</h1>
								<h2>Best in Town</h2>
								<p>If you have any aesthetic issues, we are here to help.</p>
							</div>
						</div>
					</div>
				</div>
				<a class="home-carousel-left" href="#home-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
				<a class="home-carousel-right" href="#home-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
			</div>
		</div> <!--/#home-carousel-->
    </section>
	<!-- /SLIDER -->


	<!-- ABOUT US -->
	<?php include_once("aboutUs.php"); ?>
	<!-- /ABOUT US -->


	<!-- PRICES -->
	<?php include_once("prices.php"); ?>
	<!--/PRICES  -->


	<!-- CONTACT -->
	<?php include_once("contact.php"); ?>
	<!-- /CONTACT -->

	<!-- OUR TEAM -->
	<?php include_once("ourTeam.php"); ?>
	<!-- /OUR TEAM -->






	<!-- FOOTER -->
	<footer id="footer">
		<div class="container">
			<div class="row">
				<!-- SOCIAL ICONS -->
				<div class="col-sm-6 col-sm-push-6 footer-social-icons">
					<span>follow us:</span>
					<a href="https://www.facebook.com/autoservisrobi/" target="_blank"><i class="fa fa-facebook"></i></a>
				</div>
				<!-- /SOCIAL ICONS -->
				<div class="col-sm-6 col-sm-pull-6 copyright">
					<p>designed by <i class="fa fa-love"></i><a href="https://bootstrapthemes.co">dušan i nemanja</a></p>
				</div>
			</div>
		</div>
	</footer>
	<!-- /FOOTER -->


	<!-- Scroll-up -->
	<div class="scroll-up">
		<ul><li><a href="#header"><i class="fa fa-angle-up"></i></a></li></ul>
	</div>


	<!-- JS -->
	<script type="text/javascript" src="js/jquery.min.js"></script><!-- jQuery -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script><!-- Bootstrap -->
	<script type="text/javascript" src="js/jquery.counterup.min.js"></script><!-- CounterUp -->
	<script type="text/javascript" src="js/scripts.js"></script><!-- Scripts -->
	<script type="text/javascript" src="js/map.js"></script><!-- Scripts -->
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3WkwLk4bSEJMwF2FfOUVrhqztFcHmvoA&callback=initMap" type="text/javascript"></script>


</body>
</html>
