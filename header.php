<header id="header">
  <nav class="navbar st-navbar navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#st-navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="logo" href="index.php"><img src="images/robi_svg_dynamic.png" alt=""></a>
      </div>

      <div class="collapse navbar-collapse" id="st-navbar-collapse">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#header">home</a></li>
            <li><a href="#aboutUs">about us</a></li>
            <li><a href="#prices">prices</a></li>
            <li><a href="#contact">contact</a></li>
            <li><a href="#ourTeam">our team</a></li>
            <li><?php
            $sql = "SELECT role_id FROM users ";
            $result = mysqli_query($conn, $sql) or die(mysqli_error($conn));
            if (isset($_SESSION["role_id"]) && $_SESSION["role_id"] == 1) {

                echo '<a href="admin/index.php">admin page</a>';
            } else {
                if (isset($_SESSION["role_id"]) && $_SESSION["role_id"] == 2) {


                    echo '<a href="registerYourCar.php">Register Your Car</a>';
                }
            }
            ?></li>

            <div class="nav-login" style=" margin-left: 110px;">
                <?php
                if (isset($_SESSION['u_id']) || isset($_SESSION['role_id'])) {
                    $name = $_SESSION['u_uid'];

                    echo '<form style="float: right; margin-right: 15px;" class="logoutform" action="login/includes/logout.inc.php" method="post">
            <span>you are logged in as <b>' . $name . '</b></span>
            <button type="submit" name="submit">logout</button>
            </form>';
                } else if (isset($_COOKIE['u_id']) && $_COOKIE['role_id']) {
                    $name = $_COOKIE['u_uid'];

                    echo '<form class="logoutform" action="login/includes/logout.inc.php" method="post">
            <span>you are logged in as <b>' . $name . '</b></span>
            <button type="submit" name="submit">logout</button>';
                } else {
                    echo '<form action="login/includes/login.inc.php" method="post">

            <input type="text" name="uid" placeholder="username/e-mail">
            <input type="password" name="pwd" placeholder="password">
            <button type="submit" name="submit">login</button>
            <a href="signup.php">sign up</a><br/>
            <label style="margin-top: 10px; ">Remember me</label>
          <input type="checkbox" name="remember" value="true">
          <li style="float: right;margin: 10px 0 0  0;"><a href="forgotPass.php">forgot your password?</a></li>
            </form>';
                }
                ?>

            </div> <!--end of nav-login  -->
        </ul>

      </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
  </nav>
</header>
