<?php include('login/includes/session2.inc.php'); ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">


<head>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <meta name="author" content="">

	<title>Auto Servis Robi</title>

	<!-- Main CSS file -->
	<link rel="stylesheet" href="css/bootstrap.min.css" />
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script type="text/javascript" src="node_modules/jquery-validation/dist/jquery.validate.min.js"></script>
	<link rel="stylesheet" href="css/font-awesome.css" />
	<link rel="stylesheet" href="css/style.css" />
	<link rel="stylesheet" href="css/responsive.css" />



	<!-- Favicon -->
	<link rel="shortcut icon" href="images/icon/favicon.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/icon/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/icon/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/icon/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="images/icon/apple-touch-icon-57-precomposed.png">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
    <body>
      <header id="header">
        <nav class="navbar st-navbar navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#st-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <a class="logo" href="index.php"><img src="images/robi_svg_dynamic.png" alt=""></a>
            </div>

            <div class="collapse navbar-collapse" id="st-navbar-collapse">
                <ul class="nav navbar-nav navbar-right">

                  <div  class="nav-login">

                  	<li style="margin-left: 236px;"><a  href="index.php">home</a></li>

                      <?php
                      if (isset($_SESSION['u_id']) || isset($_SESSION['role_id'])) {
                          $name = $_SESSION['u_uid'];

                          echo '<form style="float: right; margin-right: 15px;" class="logoutform" action="login/includes/logout.inc.php" method="post">
                  <span>you are logged in as <b>' . $name . '</b></span>
                  <button type="submit" name="submit">logout</button>
                  </form>';
                      } else if (isset($_COOKIE['u_id']) && $_COOKIE['role_id']) {
                          $name = $_COOKIE['u_uid'];

                          echo '<form class="logoutform" action="login/includes/logout.inc.php" method="post">
                  <span>you are logged in as <b>' . $name . '</b></span>
                  <button type="submit" name="submit">logout</button>';
                      }
                      ?>

                  </div> <!--end of nav-login  -->
              </ul>
</div><!-- /.navbar-collapse -->
          </div><!-- /.container -->
        </nav>
      </header>
        <!--car status over plates  -->
				<div class="container">
					<div class="checkYourCar">
						<h3>check your car status</h3>
							<form id="checkYourCar" action="login/includes/checkPlates.inc.php" method="post">

									<input type="text" name="plateNum" placeholder="plate number">
									<button id="checkPlate" type="submit" name="submit">submit</button>
							</form>
							<div id="checkResult" >

							</div>
							<script>
									//getting info about car by plate number
									$('#checkPlate').click(function (e) {
											e.preventDefault();
											var plateNum = $('input[name="plateNum"]').val();
											$.ajax({
													url: "login/includes/checkPlates.inc.php",
													type: "POST",
													dataType: "JSON",
													data: {
															plateNum: plateNum
													},
													success: function (response) {
															console.log(response);
															if (response.row.status) {

																	$('#checkResult').empty();
																	$('#checkResult').append('<h2>Car status: ' + response.row.status + '</h2>');

															} else {
																	$('#checkResult').empty().html(response);
															}
													}
											});
									});
							</script>
					</div>
					<form id="formReservations" action="login/includes/addProblem.inc.php" method="POST">
							<h3>report your problem</h3><br />
							<input type="text" name="ProbPlateNum" placeholder="plate number"><br />
							<legend>services</legend><br />

							<?php
							/*                 * ************LISTING REPAIRS FROM DATABASE***************** */
							$sql = "SELECT * FROM repairs";
							$result = mysqli_query($conn, $sql);
							$resultCheck = mysqli_num_rows($result);
							if ($resultCheck > 0) {
									while ($row = mysqli_fetch_array($result)) {
											$repair_id = $row['repair_id'];
											echo "<input type='checkbox' name='repair[]' value='$repair_id'>" . "<span>" . $row['name'] . ' ' . $row['price'] . "</span>" . "<br />";
									}
							}
							?>
							<br />
							<textarea type="text" name="problemDesc" rows="8" cols="60" placeholder="problem description"></textarea> <br />
							<input type="date" name="start" />
							<select>
									<option>9:00</option>
									<option>10:00</option>
									<option>11:00</option>
									<option>12:00</option>
									<option>13:00</option>
									<option>14:00</option>
							</select>
							<input type="submit" name="submit" value="Submit" />
					</form>




			<form id="carForm" class="signup-form" action="login/includes/registerYourCar.inc.php" method="POST">
					<h2>register your car</h2>
					<div class="inputs">
						<input type="text" name="carB" placeholder="car brand like audi">
						<input type="text" name="carM" placeholder="car model like A8">
						<input type="text" name="pNumber" placeholder="plate number">
						<input type="text" name="yearP" placeholder="production_year">
						<input type="text" name="displacement" placeholder="displacement">
						<input type="text" name="horsePower" placeholder="horse power"><br />

						<button type="submit" name="submit">sign up</button>
					</div>

					<script type="text/javascript">
					$("#carForm").validate({
							rules: {
									carB: {
											required: true,
											minlength: 2,
											maxlength: 10
									},
									carM: {
											required: true,
											minlength: 2,
											maxlength: 50
									},
									pNumber: {
											required: true,
											maxlength: 10
									},
									yearP: {
											required: true,
											minlength: 4,
											maxlength: 4,
									},
									displacement: {
											required: true,
											number: true,
									},
									horsePower: {
											required: true,
											number: true,
									}
							},
							messages: {
									carB: {
											required: "This is  required field",
											minlength: "Minimum length is 2 letters",
											maxlength: "Maximim length is 10 letters"
									},
									carM: {
											required: "This is  required field",
											minlength: "Minimum length is 2 letters",
											maxlength: "Maximim length is 50 letters"
									},
									pNumber: {
											required: "This is  required field",
											maxlength: "Maximim length is 10 letters"
									},
									yearP: {
											required: "This is  required field",
											minlength: "Minimum length is 4 letters",
											maxlength: "Maximim length is 4 letters"
									},
									displacement: {
											required: "This is  required field",
											number: "only numbers allowed"
									},
									horsePower: {
											required: "This is  required field",
											number: "only numbers allowed"

									}
							}
					});
					</script>
					<?php
					if (!isset($_GET['registerYourCar'])) {
							exit();
					} else {
							$signupCheck = $_GET['registerYourCar'];
							if ($signupCheck == "empty") {
									echo "<p class='errorr'>you didn't fill out all fields!</p>";
									exit();
							} elseif ($signupCheck == "char") {
									echo "<p class='errorr'>you  used invalid characters for car brand or car model!</p>";
									exit();
							} elseif ($signupCheck == "char1") {
									echo "<p class='errorr'>you  can enter only numbers for production year displacement and horse power!</p>";
									exit();
							} elseif ($signupCheck == "u_sucsess") {
									echo "<p class='sucsess'>you have been signed up!</p>";
									exit();
							}
					}
					?>
			</form>
				</div>


        <!-- submit your repair -->


        <!-- odabir slobodnog temina -->
        <!-- JS -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      	<script type="text/javascript" src="js/bootstrap.min.js"></script><!-- Bootstrap -->





    </body>
</html>
