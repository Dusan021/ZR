
    <!-- ABOUT US -->
    <div id="aboutUs" >

    </div>
  	<section style="margin-top: 70px;">
  		<div class="container">
  			<div class="row">
  				<div class="col-md-12">
  					<div class="section-title">
  						<h1>about us</h1>
  						<span class="st-border"></span>
  					</div>
  				</div>

  				<div class="col-md-4 col-sm-6 st-service">
  					<h2><i class="fa fa-car"></i> mechanic issues</h2>
  					<p>we are specialized in all kinds of mechanical issues regarding your vehicle</p>
  				</div>

  				<div class="col-md-4 col-sm-6 st-service">
  					<h2><i class="fa fa-cog fa-spin"></i>engine service</h2>
  					<p>engine replacement and customisation</p>
  				</div>

  				<div class="col-md-4 col-sm-6 st-service">
  					<h2><i class="fa fa-cog fa-spin"></i> exhaust system</h2>
  					<p>all repairs and customization for better performance</p>
  				</div>

  				<div class="col-md-4 col-sm-6 st-service">
  					<h2><i class="fa fa-cog fa-spin"></i>vulcanizer serbices</h2>
  					<p>all kinds of tire's services</p>
  				</div>

  				<div class="col-md-4 col-sm-6 st-service">
  					<h2><i class="fa fa-life-ring"></i> road support</h2>
  					<p>if u have any issues on the road, just call us and we will come to help you.</p>
  				</div>

  				<div class="col-md-4 col-sm-6 st-service">
  					<h2><i class="fa fa-shopping-cart"></i> shop </h2>
  					<p>we can sell you parts for yopur car, and if we do the service in our shop, you will get a discount on spot.</p>
  				</div>
  			</div>
  		</div>
</section>
  	<!-- /ABOUT US -->
