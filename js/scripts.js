jQuery(function($){
'use strict';
// ----------------------------------------------
    // Preloader
    // ----------------------------------------------
	(function () {
	    $(window).load(function() {
	        $('#pre-status').fadeOut();
	        $('#st-preloader').delay(350).fadeOut('slow');
	    });
	}());


	/*--------------"AJAX"-----------------*/
	(function (){
		$(document).ready(function(){
			$("#users").click(function(){
				$(".users").load("users.php", {

				});
			});
		});
	}());

	(function (){
		$(document).ready(function(){
			$("#repairs").click(function(){
				$(".users").load("repairs.php", {

				});
			});
	});	}());
		/*-----------END---"AJAX"-----------------*/

(function (){
	jQuery(document).ready(function($){
	  //get the currnet path and find taget link
	  var path = window.location.pathname.split("/").pop();

	  //account for homepage with emty path
	  if(path == ''){
	    path = 'index.php';
	  }

	  var target = $('nav li a[href="'+path+'"]');
	  // add active class to target link
	  target.addClass('active');
	});
}());


		// ----------------------------------------------
    // Fun facts
    // ----------------------------------------------
	(function () {
		$('.st-counter').counterUp({
		    delay: 10,
		    time: 1500
		});
	}());
		// -------------------------------------------------------------
    // Animated scrolling / Scroll Up
    // -------------------------------------------------------------

    (function () {
        $('li a[href*=#]').bind("click", function(e){
            var anchor = $(this);
            $('html, body').stop().animate({
                scrollTop: $(anchor.attr('href')).offset().top -79
            }, 1000);
            e.preventDefault();
        });
    }());
	// -------------------------------------------------------------
    // Back To Top
    // -------------------------------------------------------------

    (function () {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.scroll-up').fadeIn();
            } else {
                $('.scroll-up').fadeOut();
            }
        });
    }());});
