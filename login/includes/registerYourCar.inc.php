<?php session_start();
if (isset($_POST['submit'])) {
  include_once "dbh.inc.php";
  $carB = mysqli_real_escape_string($conn, $_POST['carB']);
  $carM = mysqli_real_escape_string($conn, $_POST['carM']);
  $pNumber = mysqli_real_escape_string($conn, $_POST['pNumber']);
  $yearP = mysqli_real_escape_string($conn, $_POST['yearP']);
  $displacement = mysqli_real_escape_string($conn, $_POST['displacement']);
  $horsePower = mysqli_real_escape_string($conn, $_POST['horsePower']);
  $user_id = $_SESSION['u_id'];
  $status = 'pending';
  //checking if empty
  if (empty($carB) || empty($carM) || empty($pNumber) || empty($yearP) || empty($displacement) || empty($horsePower)) {
    header("Location: ../../registerYourCar.php?registerYourCar=empty"); //err msg says empty
    exit();
  }else{
    //cheking characters
    if (!preg_match("/^[a-zA-Z\s]*$/", $carB) || !preg_match("/^[a-zA-Z0-9\s]*$/", $carM) || !preg_match("/^[a-zA-Z0-9]*$/", $pNumber) ) {
      header("Location: ../../registerYourCar.php?registerYourCar=char"); //err msg says invalid
      exit();
    }else {
      //cheking characters2
      if ( !preg_match("/^[0-9]*$/", $yearP) || !preg_match("/^[0-9]*$/", $displacement) || !preg_match("/^[0-9]*$/", $horsePower)) {
        header("Location: ../../registerYourCar.php?registerYourCar=char1"); //err msg says invalid
        exit();
      }else {
        $sql = "INSERT INTO cars (car_brand, car_model, plate_number, production_year, displacement, horse_power, user_id)
                VALUES ('$carB', '$carM', '$pNumber', '$yearP', '$displacement', '$horsePower', $user_id);";
        mysqli_query($conn, $sql);
        header("Location: ../../registerYourCar.php?registerYourCar=u_sucsess"); //err msg says sucsess
        exit();
          }
        }
      }
    }
  else {
    header("Location: Location: ../../registerYourCar.php");
    exit();
  }
?>
