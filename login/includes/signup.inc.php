<?php

session_start();
if (isset($_POST['submit'])) {
    include_once "dbh.inc.php";
    include_once "randStrGen.inc.php";

    // protection to be saved in database as string
    $first = mysqli_real_escape_string($conn, $_POST['first']);
    $last = mysqli_real_escape_string($conn, $_POST['last']);
    $email = mysqli_real_escape_string($conn, $_POST['email']);
    $uid = mysqli_real_escape_string($conn, $_POST['uid']);
    $pwd = mysqli_real_escape_string($conn, $_POST['pwd']);
    $role = 2;
    $phone = mysqli_real_escape_string($conn, $_POST['phone']);
    $zipCode = mysqli_real_escape_string($conn, $_POST['zipCode']);
    $city = mysqli_real_escape_string($conn, $_POST['city']);
    $adress = mysqli_real_escape_string($conn, $_POST['adress']);
    $date_now = date('Y-m-d H:i:s');
    // err handlers
    //check for empty fields
    if (empty($first) || empty($last) || empty($email) || empty($uid) || empty($pwd) || empty($phone)) {
        header("Location: ../../signup.php?signup=empty"); //err msg says empty
        exit();
    } else {
        //check if input char are valid
        if (!preg_match("/^[a-zA-Z]*$/", $first) || !preg_match("/^[a-zA-Z]*$/", $last)) {
            header("Location: ../../signup.php?signup=chars"); //err msg says invalid
            exit();
        } else {
            //chack if email is valid
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                header("Location: ../../signup.php?signup=email"); //err msg says invalid email
                exit();
            } else {
                //chack if username is valid
                if (!preg_match("/^[a-zA-Z0-9_-]*$/", $uid)) {
                    header("Location: ../../signup.php?signup=user"); //err msg says invalid username
                    exit();
                } else {
                    //chack if password is valid
                    if (!preg_match("/^[a-zA-Z0-9_-]*$/", $pwd)) {
                        header("Location: ../../signup.php?signup=password"); //err msg says invalid password
                        exit();
                    } else {
                        if (!preg_match("/^[a-zA-Z0-9_-]*$/", $uid)) {
                            header("Location: ../../signup.php?signup=user"); //err msg says invalid user
                            exit();
                        } else {
                            $sql = "SELECT * FROM users WHERE user_uid ='$uid' ";
                            $result = mysqli_query($conn, $sql);
                            $resultCheck = mysqli_num_rows($result);
                            if ($resultCheck > 0) {
                                header("Location: ../../signup.php?signup=usertaken"); //err msg says user taken
                                exit();
                            } else {
                                $sql = "SELECT * FROM users WHERE  user_email = '$email'";
                                $result = mysqli_query($conn, $sql);
                                $resultCheck = mysqli_num_rows($result);
                                if ($resultCheck > 0) {
                                    header("Location: ../../signup.php?signup=emailtaken"); //err msg says user taken
                                    exit();
                                } else {
                                    $sql = "SELECT * FROM users WHERE phoneNum ='$phone' ";
                                    $result = mysqli_query($conn, $sql);
                                    $resultCheck = mysqli_num_rows($result);
                                    if ($resultCheck > 0) {
                                        header("Location: ../../signup.php?signup=phone_taken"); //err msg says user taken
                                        exit();
                                    } else {
                                        //hashing the pass
                                        $hashedPwd = md5($salt1 . $pwd);
                                        //var_dump($hashedPwd);
                                        //insert the user into the database

                                        $sql = 'INSERT INTO users (user_first, user_last, user_email, user_uid, user_pwd, user_salt, reg_date,role_id,phoneNum)
                                          VALUES ("' . $first . '", "' . $last . '", "' . $email . '", "' . $uid . '", "' . $hashedPwd . '", "' . $salt1 . '", "' . $date_now . '", "' . $role . '", "' . $phone . '" );';
                                        //var_dump($sql);
                                        $test = mysqli_query($conn, $sql);
                                        $usid = mysqli_insert_id($conn);
                                        //var_dump($test);

                                        $to = "$email";
                                        $from = "auto_responder@autoservisrobi.ga";
                                        $subject = 'autoservis robi Account Activation';
                                        $message = '<!DOCTYPE html><html><head><meta charset="UTF-8"><title>autoservis robi Message</title></head><body style="margin:0px; font-family:Tahoma, Geneva, sans-serif;"><div style="padding:10px; background:#333; font-size:24px; color:#CCC;"><a href="http://autoservisrobi.ga"></a>autoservis robi Account Activation</div><div style="padding:24px; font-size:17px;">Hello ' . $uid . ',<br /><br />Click the link below to activate your account when ready:<br /><br /><a href="http://autoservisrobi.ga/activation.php?id=' . $usid . '&u=' . $uid . '&e=' . $email . '&p=' . $hashedPwd . '">Click here to activate your account now</a><br /><br />Login after successful activation using your:<br />* E-mail Address: <b>' . $email . '</b></div></body></html>';
                                        $headers = "From: $from\n";
                                        $headers .= "MIME-Version: 1.0\n";
                                        $headers .= "Content-type: text/html; charset=iso-8859-1\n";
                                        mail($to, $subject, $message, $headers);
                                        //echo "signup_success";
                                        ob_start();
                                        header("Location: ../../signup.php?signup=u_sucsess"); //err msg says sucsess
                                        ob_end_flush();
                                        exit();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
} else {
    header("Location: ../../signup.php");
    exit();
}
?>
