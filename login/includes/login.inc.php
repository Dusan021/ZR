<?php

session_start();

if (isset($_POST['submit'])) {
    include 'dbh.inc.php';
    require('randStrGen.inc.php');
    include_once "functions.inc.php";



    // protection to be saved in database as string
    $uid = mysqli_real_escape_string($conn, $_POST['uid']);
    $pwd = mysqli_real_escape_string($conn, $_POST['pwd']);
    //err handlers
    //check if inputs are empty
    if (empty($uid) || empty($pwd)) {
        header("Location: ../../index.php?login=empty");
        exit();
    } else {
        $sql3 = "SELECT active FROM users WHERE user_uid='$uid'";
        $result3 = mysqli_query($conn, $sql3);
        $resultCheck3 = mysqli_num_rows($result3);
        $row3 = mysqli_fetch_row($result3);
        // var_dump($row3[0]);
        if ($row3[0] != 1) {
            header("Location: ../../index.php?error=acivateYourAcc");




            exit();
        } else {
            $sql = "SELECT * FROM users WHERE user_uid='$uid' OR user_email = '$uid'  ";
            $result = mysqli_query($conn, $sql);
            $resultCheck = mysqli_num_rows($result);

            //var_dump($resultCheck);
            if ($resultCheck < 1) {
                header("Location: ../../index.php?login=error2");
                exit();
            } else {
                if ($row = mysqli_fetch_assoc($result)) {
                    //var_dump($row);
                    //de-hasing pass
                    $hashedPwdCheck = md5($row['user_salt'] . $pwd);
                    //var_dump($hashedPwdCheck);
                    if ($hashedPwdCheck != $row['user_pwd']) {
                        header("Location: ../../index.php?login=error3");
                        exit();
                    } elseif ($hashedPwdCheck == $row['user_pwd']) {

                        if ($_POST['remember'] == true) {
                            setcookie('rememberme', true, time() + (86400 * 30), "/");
                            setcookie('u_id', $row['user_id'], time() + (86400 * 30), "/");
                            setcookie('role_id', $row['role_id'], time() + (86400 * 30), "/");
                            setcookie('u_uid', $row['user_uid'], time() + (86400 * 30), "/");
                        }

                        $sql2 = 'SELECT * FROM cars WHERE user_id=' . $row['user_id'];
                        $result2 = mysqli_query($conn, $sql2);
                        $resultCheck2 = mysqli_num_rows($result2);
                        $row2 = mysqli_fetch_assoc($result2);
                        //log in usere here
                        $_SESSION['u_id'] = $row['user_id'];
                        $_SESSION['u_first'] = $row['user_first'];
                        $_SESSION['u_last'] = $row['user_last'];
                        $_SESSION['u_email'] = $row['user_email'];
                        $_SESSION['u_uid'] = $row['user_uid'];
                        $_SESSION['role_id'] = $row['role_id'];
                        $_SESSION['p_number'] = $row2["plate_number"];
                        $_SESSION['car_id'] = $row2["car_id"];
                        $_SESSION['logged'] = 1;

                        $_SESSION['userArray'] = array(
                            'u_id' => $row['user_id']
                        );
                        ob_start();
                        header("Location: ../../index.php?login=success");
                        ob_end_flush();
                        exit();
                    }
                }
            }
        }
    }
} else {
    header("Location: ../../index.php?login=error1");
    exit();
}
?>
