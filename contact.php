<!-- CONTACT -->

<section  id="contact">
  <div style="margin-top: 50px;" class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="section-title">
          <h1>Contact us</h1>
          <span class="st-border"></span>
        </div>
      </div>
      <div class="col-sm-4 contact-info">
        <p class="contact-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae voluptatum dolorum, possimus tenetur pariatur officia, quo commodi autem doloribus vero rerum aspernatur quidem temporibus.</p>
        <p class="st-address"><i class="fa fa-map-marker"></i> <strong>Šarplaninska 8,Telep, Novi Sad, Serbia</strong></p>
        <p class="st-phone"><i class="fa fa-mobile"></i> <strong> +381 (021) 505 101</strong></p>
        <p class="st-phone"><i class="fa fa-mobile"></i> <strong>+381 (063) 150 5101</strong></p>
        <p class="st-email"><i class="fa fa-envelope-o"></i> <strong>sitescadu@gmail.com</strong></p>

      </div>
      <div class="col-sm-7 col-sm-offset-1">
        <form action="login/includes/sendContact.inc.php" class="contact-form" name="contact-form" method="post">
          <div class="row">
            <div class="col-sm-6">
              <input type="text" name="name" required="required" placeholder="Name*">
            </div>
            <div class="col-sm-6">
              <input type="email" name="email" required="required" placeholder="Email*">
            </div>
            <div class="col-sm-6">
              <input type="text" name="subject" placeholder="Subject">
            </div>
            <div class="col-sm-6">
              <input type="text" name="website" placeholder="Website">
            </div>
            <div class="col-sm-12">
              <textarea name="message" required cols="30" rows="7" placeholder="Message*"></textarea>
            </div>
            <div class="col-sm-12">
              <input type="submit" name="submit" value="Send Message" class="btn btn-send">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div id="map">

  </div>
</section>


<!-- /CONTACT -->
